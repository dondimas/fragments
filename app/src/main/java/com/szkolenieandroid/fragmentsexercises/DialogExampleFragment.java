package com.szkolenieandroid.fragmentsexercises;



import android.app.DialogFragment;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



/**
 * A simple {@link Fragment} subclass.
 *
 */
public class DialogExampleFragment extends DialogFragment {


    public DialogExampleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("My Dialog Title");
        View dialodView = inflater.inflate(R.layout.fragment_dialog_example, container, false);
        dialodView.findViewById(R.id.close_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogExampleFragment.this.dismiss();
            }
        });
        return dialodView;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(300,300);
    }
}
